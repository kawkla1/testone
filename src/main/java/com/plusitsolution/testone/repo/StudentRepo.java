package com.plusitsolution.testone.repo;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

import com.plusitsolution.testone.es.StudentEntity;

@Repository
public interface StudentRepo extends ElasticsearchRepository<StudentEntity, String> {
	
	public List<StudentEntity> findAll();
	public List<StudentEntity> findByDate(LocalDateTime date);
	public List<StudentEntity> findByName(String name);
	
}
