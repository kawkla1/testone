package com.plusitsolution.testone.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.plusitsolution.testone.domain.StudentDomain;
import com.plusitsolution.testone.es.StudentEntity;
import com.plusitsolution.testone.repo.StudentRepo;

@Service
public class StudentService {
	
	@Autowired
	private StudentRepo studentRepo;
	
//	public StudentDomain addStudent(String name,String id) {
//		StudentDomain studentDomain = new StudentDomain();
//		studentDomain.setName(name);
//		studentDomain.setId(id);
//		return studentDomain;
//	}
	
//	public StudentDomain getAll() {
//		StudentDomain studentDomain = new StudentDomain();
//		studentDomain.setName("sdsa");
//		studentDomain.setId("ID");
//		
//		return studentDomain;
//	}
	
	public StudentEntity addStudent(String name,String id) {
		StudentEntity studentEntity = new StudentEntity();
		studentEntity.setName(name);
		studentEntity.setId(id);
		return studentRepo.save(studentEntity);
	}
	
	public List<StudentEntity> getAll() {
		
		
		return studentRepo.findAll();
	}

}
