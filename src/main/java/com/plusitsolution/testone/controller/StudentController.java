package com.plusitsolution.testone.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.plusitsolution.testone.domain.StudentDomain;
import com.plusitsolution.testone.es.StudentEntity;
import com.plusitsolution.testone.service.StudentService;

@RestController
public class StudentController {
	
	@Autowired
	StudentService service;
	
//	@PostMapping("/name")
//	public StudentDomain addStu(@RequestParam("name") String name,@RequestParam("id") String id){
//		
//		return service.addStudent(name, id);
//	}
	
	@PutMapping("/name")
	public StudentEntity addStu2(@RequestBody StudentDomain studentDomain){
		
		return service.addStudent(studentDomain.getName(),studentDomain.getId());
	}
	
//	@GetMapping("/getstu")
//	public StudentDomain getStu() {
//		return service.getAll();
//	}
	
	@GetMapping("/getstu")
	public List<StudentEntity> getStu() {
		return service.getAll();
	}
	
	
	
	

}
