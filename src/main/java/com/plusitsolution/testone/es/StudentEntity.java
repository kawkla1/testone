package com.plusitsolution.testone.es;

import java.time.LocalDateTime;

import org.joda.time.LocalDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.DateFormat;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import com.plusitsolution.testone.enums.Gender;

@Document(indexName = "student-index")
public class StudentEntity {
	
	private Gender gender;
	
	@Field(type = FieldType.Keyword)
	private String name;
	
	@Field(type = FieldType.Date, format = DateFormat.basic_date_time)
	private LocalDateTime date;
	
	@Id
	private String id;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public LocalDateTime getDate() {
		return date;
	}

	public void setDate(LocalDateTime date) {
		this.date = date;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	
	
	
	
}
